#ifndef CONV_H_
#define CONV_H_

#include <ctype.h>
#include <inttypes.h>

int8_t str2uint8(const char *str, uint8_t *value);
int8_t str2uint16(const char *str, uint16_t *value);

#endif /* CONV_H_ */
