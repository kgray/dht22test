#ifndef DHT22TEST_H_
#define DHT22TEST_H_

#include <avr/pgmspace.h>

// ****************************************************************************
// Constants
// ****************************************************************************

#define VERSION					0.1
#define VERSION_STRING			"0.1"
#define	UART_BAUD_RATE			115200

// Ports

// Time intervals
//#define TCNT1_INIT			-7200
#define TCNT1_INIT				-14400

// Work flags
#define	WF_1S					0

// Variables

#endif /* DHT22TEST_H_ */
