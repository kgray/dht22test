/*****************************************************************************
 * Name       : AT CMD
 * Version    : 0.5
 * Description: Hayes-compatible AT-commands
 *****************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <ctype.h>
#include <inttypes.h>
#include "atcmd.h"
#include "uart.h"

// ****************************************************************************
// Global definitions
// ****************************************************************************

#define atcmd_buffer_SIZE	40
#define UART_ERROR	(UART_FRAME_ERROR|UART_OVERRUN_ERROR|UART_BUFFER_OVERFLOW)

// ****************************************************************************
// Global variables
// ****************************************************************************

char atcmd_buffer[atcmd_buffer_SIZE];
uint8_t atcmd_index = 0;

// ****************************************************************************
// Functions
// ****************************************************************************

int8_t atcmd_parse()
{
	unsigned int c;
	char ch;
	static char prev_ch = '\0';
	static int8_t at_present = 0;

	while ( !((c = uart_getc()) & (UART_NO_DATA|UART_ERROR)) && atcmd_index < atcmd_buffer_SIZE )
	{
		ch = (char)(c&0xff);

		if ( ch == '\b' || ch == '\x7f' )
		{
			uart_putc('\b');
			uart_putc(' ');
			uart_putc('\b');
			if ( at_present && atcmd_index )
				atcmd_index--;
		}
		else
		if ( !at_present )
		{
			if ( isalnum(ch) || ch == '\r' )
				uart_putc(ch);	// Echo
			if ( (prev_ch == 'A' || prev_ch == 'a') && (ch == 'T' || ch == 't') )
				at_present = 1;
		}
		else
		if ( ch == '\r' ) // at_present && '\r'
		{
			if ( atcmd_index < atcmd_buffer_SIZE )
			{
				atcmd_buffer[atcmd_index] = '\0';
				atcmd_index = atcmd_buffer_SIZE;	// No more symbols can be accepted
				at_present = 0;
			}
		}
		else // at_present && !'\r'
		if ( isprint(ch) && atcmd_index < atcmd_buffer_SIZE-1 )
		{
			atcmd_buffer[atcmd_index++] = ch;
			uart_putc(ch);	// Echo
		}
		prev_ch = ch;
	}

	return !(atcmd_index < atcmd_buffer_SIZE);
}

void atcmd_empty()
{
	atcmd_index = 0;
}
