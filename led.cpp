#include <avr/io.h>
#include <util/delay.h>
#include "led.h"

void led_init()
{
	//	Init Ports
	LED_DDR |=  LED_MASK;
	LED_PORT &= ~LED_MASK;
}	

void led( uint8_t enable )
{
	if ( enable )
		LED_PORT |= _BV(LED_PN0);
	else
		LED_PORT &= ~_BV(LED_PN0);
}

void led_blink()
{
	led(1);
	_delay_ms(250);
	led(0);
}
