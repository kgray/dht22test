#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <ctype.h >
#include "uart.h"
//#include "atcmd.h"
#include "messages.h"
#include "led.h"
#include "conv.h"
#include "dht22.h"
#include "commands.h"

// Handlers
const command_handler_item_t handlers_table[] PROGMEM =
	{
		{ "Z",   1, handler_reset },
		{ "I3",  2, handler_info3 },
		{ "I4",  2, handler_info4 },
		{ "I5",  2, handler_info5 },
		{ "L",   1, handler_led },
		{ "HT",  2, handler_dht22 }
	};

const uint8_t handlers_table_size = sizeof(handlers_table) / sizeof(handlers_table[0]);

// Functions
int8_t process_command( char *command )
{
	command_handler_ptr_t handler;
	uint8_t n;
	uint8_t cmd_len;

	if ( command[0] == '\0' )
		return handler_null();
	for ( n=0; n<handlers_table_size; n++ )
	{
		cmd_len = pgm_read_byte(&handlers_table[n].command_length);
		if ( !strncasecmp_P(command,handlers_table[n].command,cmd_len) )
		{
			break;
		}			
	}
	if ( n<handlers_table_size )
	{
		memcpy_P(&handler,&handlers_table[n].handler,sizeof(handler));
		return handler(command+cmd_len);
	}

	return 1;
}

// Command AT<CR> - just for OK response
int8_t handler_null()
{
	uart_puts_p(msg_ok);
	return 0;
}

// ATZ
int8_t handler_reset( char *command )
{
	uart_puts_p(msg_ok);

	wdt_enable(WDTO_15MS);
	while (1);

	return 0;
}

// ATI3
int8_t handler_info3( char *command )
{
	if ( command[0] != '\0' )
		return 1;
	uart_puts_p(msg_intro);
	return 0;
}

// ATI4
int8_t handler_info4( char *command )
{
	if ( command[0] != '\0' )
		return 1;
	uart_puts_p(msg_info4);
	return 0;
}

// ATI5
int8_t handler_info5( char *command )
{
	if ( command[0] != '\0' )
		return 1;
	uart_puts_p(msg_info5);
	return 0;
}

// ATL[0|1]
int8_t handler_led( char *command )
{
	if ( command[0] == '1' )
		led(1);
	else
	if ( command[0] == '0' )
		led(0);
	else
		return 1;

	uart_puts_p(msg_ok);
	return 0;
}

// ATHT
int8_t handler_dht22( char *command )
{
	uint8_t rh_int, rh_dec, t_int, t_dec;
	char str[16];

	if ( command[0] != '\0' )
		return 1;

	if ( dht22_get_data(rh_int,rh_dec,t_int,t_dec) )
		uart_puts_p(msg_sensor_failed);
	else
	{
		uart_puts_p(msg_dht22_data);
		snprintf(str,16,"%02X-%02X-",rh_int,rh_dec);
		uart_puts(str);
		snprintf(str,16,"%02X-%02X",t_int,t_dec);
		uart_puts(str);
		uart_puts_p(msg_newline);

		uint16_t rh = (rh_int<<8) | rh_dec;
		snprintf(str,16,"RH: %d.%d %%\r\n",rh/10,rh%10);
		uart_puts(str);

		uint16_t temp = ((t_int & 0x7F) << 8) | t_dec;
		uart_puts("T: ");
		if ( t_int & 0x80 )
			uart_puts("-");
		snprintf(str,16,"%d.%d *C\r\n",temp/10,temp%10);
		uart_puts(str);
	}

	uart_puts_p(msg_ok);
	return 0;
}
