#ifndef _ATCMD_H
#define _ATCMD_H

#include <inttypes.h>

// ****************************************************************************
// Constant definitions
// ****************************************************************************

#define UART_STRING_SIZE	40
#define UART_ERROR			(UART_FRAME_ERROR|UART_OVERRUN_ERROR|UART_BUFFER_OVERFLOW)

// ****************************************************************************
// Global variables
// ****************************************************************************

extern char atcmd_buffer[UART_STRING_SIZE];
extern uint8_t atcmd_index;

// ****************************************************************************
// Functions
// ****************************************************************************
int8_t atcmd_parse();
void atcmd_empty();

#endif // _ATCMD_H
