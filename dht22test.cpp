/*****************************************************************************
 *
 * Name: DHT22 Test
 * Version: 0.1
 * Date: 7 Dec 2014
 * Description: Test DHT22 sensor
 * uC: ATmega32A @ 7.372800 MHz
 *
 *****************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
//#include <util/delay_basic.h>
#include <util/delay.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include "uart.h"
#include "atcmd.h"
#include "commands.h"
#include "messages.h"
#include "led.h"
#include "dht22.h"
#include "dht22test.h"

// ****************************************************************************
// Variables
// ****************************************************************************

static uint8_t wflags = 0;

// ****************************************************************************
// Interrupt handler
// ****************************************************************************

// Timer/Counter1 Overflow
ISR(TIMER1_OVF_vect)
{
	TCNT1 = TCNT1_INIT;
	wflags |= _BV(WF_1S);
}

// ****************************************************************************
// Functions
// ****************************************************************************

void led_intro()
{
	led(1);
	_delay_ms(100);
	led(0);
	_delay_ms(100);
	led(1);
	_delay_ms(100);
	led(0);
	_delay_ms(100);
	led(1);
	_delay_ms(100);
	led(0);
}

void init()
{
	// Init variables

	// Init Ports
	led_init();
	dht22_init();

	// Init timers
	TCNT1 = TCNT1_INIT;
	TCCR1B = _BV(CS12) | _BV(CS10);	// F_clk/1024
	TIMSK = _BV(TOIE1);

	// Init UART
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	sei(); // Go!
}

int main( void )
{
	init();
	led_intro();
	uart_puts_p(msg_intro);

	while( 1 )
	{
/*
		if ( wflags & _BV(WF_1S) )
		{
			wflags &= !_BV(WF_1S);
			if ( dac_state )
			{
				dac8512_set_value(0x0FFF);
				dac_state = 0;
				led_blink();
			}
			else
			{
				dac8512_set_value(0x00F);
				dac_state = 1;
				led_blink();
				led_blink();
			}
		}
*/

		if ( atcmd_parse() )
		{
			atcmd_empty();
			uart_puts_p(msg_newline);
			if ( process_command(atcmd_buffer) )
				uart_puts_p(msg_error);
			uart_puts_p(msg_newline);
		}
	} // while (1)

	return 0;
}
