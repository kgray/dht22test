#ifndef MESSAGES_H_
#define MESSAGES_H_

#include <avr/pgmspace.h>
#include <ctype.h>
#include <inttypes.h>

extern const char msg_intro[] PROGMEM;
extern const char msg_signature[] PROGMEM;
extern const char msg_ok[] PROGMEM;
extern const char msg_newline[] PROGMEM;
extern const char msg_error[] PROGMEM;
extern const char msg_info4[] PROGMEM;
extern const char msg_info5[] PROGMEM;
extern const char msg_data[] PROGMEM;
extern const char msg_sensor_failed[] PROGMEM;
extern const char msg_dht22_data[] PROGMEM;
extern const char msg_bye[] PROGMEM;

#endif /* MESSAGES_H_ */
