/*
 * dht22.h
 *
 *  Created on: 07 дек. 2014 г.
 *      Author: gray
 */

#ifndef DHT22_H_
#define DHT22_H_

#include <avr/io.h>

#define DHT22_DDR		DDRD
#define DHT22_PORT		PORTD
#define DHT22_PIN		PIND
#define	DHT22_PN		PD2

void dht22_init();
uint8_t dht22_get_data(uint8_t &rh_int, uint8_t &rh_dec, uint8_t &t_int, uint8_t &t_dec);

#endif /* DHT22_H_ */
