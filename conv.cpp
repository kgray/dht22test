#include <avr/io.h>
#include "conv.h"

int8_t str2uint8( const char *str, uint8_t *value )
{
    *value = 0;

    while ( *str == '0' && *(str+1) != '\0' )
		str++;

    if ( !isdigit(str[0]) )
		return -1;
    *value = str[0] - '0';

    if ( str[1] == '\0' )
		return 0;
    if ( !isdigit(str[1]) )
		return -1;
    *value = *value * 10 + (str[1] - '0');

    if ( str[2] == '\0' )
		return 0;
    if ( *value > 25 )
		return -1;
    if ( !isdigit(str[2]) || str[3] != '\0' )
		return -1;
    if ( *value == 25 && str[2] > '5' )
		return -1;
    *value = *value * 10 + (str[2]-'0');

    return 0;
}

int8_t str2uint16( const char *str, uint16_t *value )
{
	*value = 0;

	while ( *str == '0' && *(str+1) != '\0' )
		str++;

	if ( !isdigit(str[0]) )
		return -1;
	*value = str[0] - '0';

	for ( int n=1; n<4; n++ )
	{
		if ( str[n] == '\0' )
			return 0;
		if ( !isdigit(str[n]) )
			return -1;
		*value = *value * 10 + (str[n] - '0');
	}		

	if ( str[4] == '\0' )
		return 0;
	if ( *value > 6553 )
		return -1;
	if ( !isdigit(str[4]) || str[5] != '\0' )
		return -1;
	if ( *value == 6553 && str[4] > '5' )
		return -1;
	*value = *value * 10 + (str[4]-'0');

	return 0;
}
