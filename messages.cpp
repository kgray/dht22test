#include <avr/io.h>
#include <avr/pgmspace.h>
#include <ctype.h>
#include <inttypes.h>
#include "messages.h"
#include "dht22test.h"

// Messages
const char msg_intro[] PROGMEM = "DHT22 Test " VERSION_STRING "\r\n";
const char msg_ok[] PROGMEM = "OK\r\n";
const char msg_newline[] PROGMEM = "\r\n";
const char msg_error[] PROGMEM = "ERROR\r\n";
const char msg_info4[] PROGMEM = "Built on " __DATE__ " at " __TIME__ "\r\n";
const char msg_info5[] PROGMEM = "STATUS: OK\r\n";
const char msg_data[] PROGMEM = "DATA:";
const char msg_sensor_failed[] PROGMEM = "SENSOR FAILED\r\n\r\n";
const char msg_dht22_data[] PROGMEM = "DHT22 DATA: ";
const char msg_bye[] PROGMEM = "Bye\r\n\r\n";
