#include "dht22.h"
#include <avr/interrupt.h>
//#include <util/delay_basic.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "messages.h"
#include "uart.h"

static volatile uint8_t timer_is_running = 0;
static volatile uint8_t buffer[5];

// Timer/Counter1 Overflow
ISR(TIMER0_OVF_vect)
{
	TCCR0 = 0;	// Stop timer
	timer_is_running = 0;
}

void dht22_init()
{
	DHT22_DDR &= ~_BV(DHT22_PN);
	DHT22_PORT &= ~_BV(DHT22_PN);
}

inline void dht22_line_take()
{
	DHT22_DDR |= _BV(DHT22_PN);
}

inline void dht22_line_release()
{
	DHT22_DDR &= ~_BV(DHT22_PN);
}

uint8_t dht22_wait_for_high(uint8_t wait_time)
{
	// Start timer
	TCNT0 = 0x100-wait_time;
	TIMSK |= _BV(TOIE0);
	TIFR |= _BV(TOV0);
	timer_is_running = 1;
	TCCR0 |= _BV(CS01);	// Start timer with clk/8

	while ( !(DHT22_PIN & _BV(DHT22_PN)) && timer_is_running ) ;
	if ( !timer_is_running )
		return 1;	// Line state was not changed during timer period

	TCCR0 = 0;		// Stop timer
	return 0;
}

uint8_t dht22_wait_for_low(uint8_t wait_time)
{
	// Start timer
	TCNT0 = 0x100-wait_time;
	TIMSK |= _BV(TOIE0);
	TIFR |= _BV(TOV0);
	timer_is_running = 1;
	TCCR0 |= _BV(CS01);	// Start timer with clk/8

	while ( (DHT22_PIN & _BV(DHT22_PN)) && timer_is_running ) ;
	if ( !timer_is_running )
		return 1;	//	Line state was not changed during timer period

	TCCR0 = 0;		// Stop timer
	return 0;
}

uint8_t dht22_get_data(uint8_t &rh_int, uint8_t &rh_dec, uint8_t &t_int, uint8_t &t_dec)
{
	// Start signal
	dht22_line_take();
	_delay_ms(15);
	dht22_line_release();
	if ( dht22_wait_for_low(40) )	// Wait for LOW 40 us
		return 1;

	// Wait for sensor to start
	if ( dht22_wait_for_high(80) )	// Wait for HIGH 80 us
		return 1;
	if ( dht22_wait_for_low(80) )	// Wait for LOW 80 us
		return 1;

	// Acquire data
	for ( uint8_t m=0; m<sizeof(buffer)/sizeof(buffer[0]); ++m )
	{
		uint8_t mask = 0x80;
		buffer[m] = 0;
		for ( int n=0; n<8; n++ )
		{
			if ( dht22_wait_for_high(60) )
				return 1;
			_delay_us(50);
			if ( DHT22_PIN & _BV(DHT22_PN) )
			{
				buffer[m] |= mask;
				if ( dht22_wait_for_low(30) )
					return 1;
			}
			mask >>= 1;
		}
	}

	if ( uint8_t(buffer[0] + buffer[1] + buffer[2] + buffer[3]) - buffer[4] != 0 )
		return 2;

	rh_int = buffer[0];
	rh_dec = buffer[1];
	t_int =  buffer[2];
	t_dec =  buffer[3];

	return 0;
}
