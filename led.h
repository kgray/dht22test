#ifndef LED_H_
#define LED_H_

#include <avr/io.h>
#include <ctype.h>
#include <inttypes.h>

#define LED_DDR					DDRC
#define LED_PORT				PORTC
#define LED_PN0					PC0
#define LED_MASK				_BV(LED_PN0)

void led_init();
void led( uint8_t enable );
void led_blink();

#endif /* LED_H_ */
