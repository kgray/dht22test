#if !defined(_COMMANDS_H)
#define _COMMANDS_H 1

#include <avr/pgmspace.h>

// ****************************************************************************
// Constants
// ****************************************************************************

#define COMMAND_MAX_LEN	3

// ****************************************************************************
// Type definitions
// ****************************************************************************

typedef int8_t (*command_handler_ptr_t)(char *);

typedef struct
{
	const char command[COMMAND_MAX_LEN+1];
//	const char *command;
	uint8_t command_length;
	command_handler_ptr_t handler;

} command_handler_item_t;

// ****************************************************************************
// Global variables
// ****************************************************************************

// Messages
extern const char msg_ok[] PROGMEM;
extern const char msg_newline[] PROGMEM;
extern const char msg_error[] PROGMEM;
extern const char msg_info3[] PROGMEM;
extern const char msg_info4[] PROGMEM;
extern const char msg_info5[] PROGMEM;

// Handler variables
extern const command_handler_item_t handlers_table[] PROGMEM;
extern const uint8_t handlers_table_size;

// ****************************************************************************
// Prototypes
// ****************************************************************************

int8_t process_command( char *command );
int8_t handler_null();
int8_t handler_reset( char *command );
int8_t handler_info3( char *command );
int8_t handler_info4( char *command );
int8_t handler_info5( char *command );
int8_t handler_led( char *command );
int8_t handler_dht22( char *command );

#endif // !defined(_COMMANDS_H)
